import * as THREE from "./three.module.js"
import { scene, camera } from "./main.js"


createAxes();


function createAxes(xName = " xAxis, m", yName = " yAxis, m", zName = " zAxis, m",
    xColor = 0x0000ff, yColor = 0x0000ff, zColor = 0x0000ff,
    length = 30) {

    const xMaterial = new THREE.LineBasicMaterial({ color: xColor });
    const yMaterial = new THREE.LineBasicMaterial({ color: yColor });
    const zMaterial = new THREE.LineBasicMaterial({ color: zColor });

    const xPoints = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(length, 0, 0)];
    const yPoints = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, length, 0)];
    const zPoints = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, length)];

    const xGeometry = new THREE.BufferGeometry().setFromPoints(xPoints);
    const yGeometry = new THREE.BufferGeometry().setFromPoints(yPoints);
    const zGeometry = new THREE.BufferGeometry().setFromPoints(zPoints);

    const xAxis = new THREE.Line(xGeometry, xMaterial);
    const yAxis = new THREE.Line(yGeometry, yMaterial);
    const zAxis = new THREE.Line(zGeometry, zMaterial);

    let xSprite = makeTextSprite(xName);
    let ySprite = makeTextSprite(yName);
    let zSprite = makeTextSprite(zName);

    xSprite.position.set(length * 0.9, 0, 0);
    ySprite.position.set(0, length * 0.9, 0);
    zSprite.position.set(0, 0, length * 0.9);


    scene.add(xAxis, yAxis, zAxis);

    //scene.add(xSprite, ySprite, zSprite);
}

function makeTextSprite(message, parameters) {
    if (parameters === undefined) parameters = {};
    var fontface = parameters.hasOwnProperty("fontface") ? parameters["fontface"] : "Arial";
    var fontsize = parameters.hasOwnProperty("fontsize") ? parameters["fontsize"] : 18;
    var borderThickness = parameters.hasOwnProperty("borderThickness") ? parameters["borderThickness"] : 4;
    var borderColor = parameters.hasOwnProperty("borderColor") ? parameters["borderColor"] : { r: 0, g: 0, b: 0, a: 1.0 };
    var backgroundColor = parameters.hasOwnProperty("backgroundColor") ? parameters["backgroundColor"] : { r: 255, g: 255, b: 255, a: 1.0 };
    var textColor = parameters.hasOwnProperty("textColor") ? parameters["textColor"] : { r: 0, g: 0, b: 0, a: 1.0 };

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.font = "Bold " + fontsize + "px " + fontface;
    var metrics = context.measureText(message);
    var textWidth = metrics.width;

    context.fillStyle = "rgba(" + backgroundColor.r + "," + backgroundColor.g + "," + backgroundColor.b + "," + backgroundColor.a + ")";
    context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + "," + borderColor.b + "," + borderColor.a + ")";

    context.lineWidth = borderThickness;
    roundRect(context, borderThickness / 2, borderThickness / 2, (textWidth + borderThickness) * 1.1, fontsize * 1.4 + borderThickness, 8);

    context.fillStyle = "rgba(" + textColor.r + ", " + textColor.g + ", " + textColor.b + ", 1.0)";
    context.fillText(message, borderThickness, fontsize + borderThickness);

    var texture = new THREE.Texture(canvas)
    texture.needsUpdate = true;

    var spriteMaterial = new THREE.SpriteMaterial({ map: texture});
    var sprite = new THREE.Sprite(spriteMaterial);
    sprite.scale.set(0.5 * fontsize, 0.25 * fontsize, 0.75 * fontsize);
    return sprite;
}

function roundRect(ctx, x, y, w, h, r) {
    ctx.beginPath(); ctx.moveTo(x + r, y);
    ctx.lineTo(x + w - r, y);
    ctx.quadraticCurveTo(x + w, y, x + w, y + r)
    ctx.lineTo(x + w, y + h - r);
    ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
    ctx.lineTo(x + r, y + h);
    ctx.quadraticCurveTo(x, y + h, x, y + h - r);
    ctx.lineTo(x, y + r);
    ctx.quadraticCurveTo(x, y, x + r, y);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
}

