const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Testing');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

const readLine = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readLine.question('', input => {
    if (input == "end") {
        process.exit();
    }
    if (input == "convertFile") {
        convertFile();
    }
});


function convertFile() {

    let fs = require('fs');
    let readLine = require('readline');
    let stream = require('stream');

    let instream = fs.createReadStream('../heightmap/54752_dem.xyz');
    let outsream = new stream();
    let rl = readLine.createInterface(instream, outsream);

    let lineCount = 0;
    let xOffset;
    let yOffset;


    rl.on('line', function (line) {

        let _data = line.split(" ");

        if (lineCount == 0) {
            xOffset = _data[0];
            yOffset = _data[1];
        }

        let x = _data[0] - xOffset;
        let y = (_data[1] - yOffset) * -1;
        let z = _data[2];

        let data = "{ x: " + x + ", y: " + y + ", z: " + z + "},\n";

        if (lineCount % 50000 === 0) {
            console.log('Proccessed lines: ' + lineCount);

        }

        fs.appendFile("data.json", data, (err) => {
            if (err) console.log(err);
        });

        lineCount++;
    });

    rl.on('close', function () {

/*        let jsonString = JSON.stringify(points);

        fs.writeFile("data.json", jsonContent, 'utf8', function (err) {
            if (err) {
                console.log("An error occured while writing JSON Object to File.");
                return console.log(err);
            }
            console.log("JSON file has been saved.");
        });*/

    });
}
