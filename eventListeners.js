import { camera, renderer, scene } from "./main.js"
import * as THREE from "./three.module.js"

window.addEventListener('resize', onWindowResize, false);
window.addEventListener('mousedown', onPointerClick);

const raycaster = new THREE.Raycaster();
const pointer = new THREE.Vector2();

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onPointerClick() {

    pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    pointer.y = - (event.clientY / window.innerHeight) * 2 + 1;

    //raycaster.setFromCamera(pointer, camera);
    //const intersects = raycaster.intersectObjects(scene.children);

    //console.log(scene.children);

/*    let position = intersects[0].point
    let pointGeometry = new THREE.BufferGeometry().setFromPoints([position]);
    let pointMaterial = new THREE.PointsMaterial({ color: 0x00ff00, size: 3, sizeAttenuation: false });
    let point = new THREE.Points(pointGeometry, pointMaterial);

    scene.add(point);*/
    
}