import { pointSet, scene, generationSteps } from "./main.js"

import * as THREE from "./three.module.js"

let xLineSet = [];
let yLineSet = [];

/*function generatePoligon(point1, point2, point3, color, opacity) {

    let points = new Float32Array([
        point1.x, point1.y, point1.z,
        point2.x, point2.y, point2.z,
        point3.x, point3.y, point3.z
    ]);


    let geometry = new THREE.BufferGeometry();
    geometry.setAttribute('position', new THREE.BufferAttribute(points, 3));

    const material = new THREE.MeshBasicMaterial({ color: color, opacity: opacity, transparent: true });
    const mesh = new THREE.Mesh(geometry, material);

    scene.add(mesh);

}

function drawLine(point1, point2, color = 0xff0000) {

    const material = new THREE.LineBasicMaterial({ color: color });
    const points = [point1, point2];
    const geometry = new THREE.BufferGeometry().setFromPoints(points);

    const line = new THREE.Line(geometry, material);
    scene.add(line);
   
}*/


/*export function createPoligons() {

    for (let i = 0; i < generationSteps - 1; i++) {
        for (let j = 0; j < generationSteps - 1; j++) {
            let point1 = pointSet[i + generationSteps * j];
            let point2 = pointSet[i + 1 + generationSteps * j];
            let point3 = pointSet[generationSteps + i + 1 + generationSteps * j];
            let point4 = pointSet[generationSteps + i + generationSteps * j];
            generatePoligon(point1, point2, point3, Math.random() * 0xffffff, 0.1);
            generatePoligon(point1, point3, point4, Math.random() * 0xffffff, 0.1);
        }
    }
}*/


//This one draws lines to simulate poligons


export function createPoligons() {
        for (let i = 0; i < generationSteps - 1; i++) {
            for (let j = 0; j < generationSteps - 1; j++) {
                let point1 = pointSet[i * generationSteps + j];
                let point2 = pointSet[i * generationSteps + j + 1];
                let point3 = pointSet[j * generationSteps + i];
                let point4 = pointSet[(j+1) * generationSteps + i];
                xLineSet.push(point1);
                xLineSet.push(point2);
                yLineSet.push(point3);
                yLineSet.push(point4);

            }
    }


    let xLineGeometry = new THREE.BufferGeometry().setFromPoints(xLineSet);
    let xLineMaterial = new THREE.LineBasicMaterial({ color: 0x00ff00, transparent: true, opacity: 0.25 });
    let xLines = new THREE.LineSegments(xLineGeometry, xLineMaterial);
    scene.add(xLines);

    let yLineGeometry = new THREE.BufferGeometry().setFromPoints(yLineSet);
    let yLineMaterial = new THREE.LineBasicMaterial({ color: 0x00ff00, transparent: true, opacity: 0.25 });
    let yLines = new THREE.LineSegments(yLineGeometry, yLineMaterial);
    scene.add(yLines);
}
