import * as THREE from "./three.module.js"
import { OrbitControls } from "./OrbitControls.js"
import { createPoligons } from "./poligonizer.js"
//import { points } from "../heightmap/data.json"

export let scene, camera, renderer;
export let pointSet = [];
export let generationSteps = 1000;

let createGeometry = function () {

    let size = 20;
    let stepSize = size / generationSteps;

    for (let i = 0; i < generationSteps; i++) {
        for (let j = 0; j < generationSteps; j++) {
            pointSet.push(new THREE.Vector3(stepSize * i, Math.sin(stepSize * (i * i + j * j))/10, stepSize * j));
        }
    }

    let setgeometry = new THREE.BufferGeometry().setFromPoints(pointSet);
    let setmaterial = new THREE.PointsMaterial({ color: 0xff0000, size: 2, sizeAttenuation: false });
    let plot = new THREE.Points(setgeometry, setmaterial);

    scene.add(plot);

    createPoligons();
}


/*let createCity = function () {

    points.forEach(point => {
        pointSet.push(new THREE.Vector3(point.x / 100, point.y / 100, point.z / 10));
    });

    let setgeometry = new THREE.BufferGeometry().setFromPoints(pointSet);
    let setmaterial = new THREE.PointsMaterial({ color: 0xff0000, size: 2, sizeAttenuation: false });
    let plot = new THREE.Points(setgeometry, setmaterial);

    scene.add(plot);

}
*/
let init = function () {
    scene = new THREE.Scene;
    scene.background = new THREE.Color(0x000000);
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);

    camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 1, 1000);
    let controls = new OrbitControls(camera, renderer.domElement);
    const light = new THREE.AmbientLight(0x404040);

    camera.position.set(-4, 4, 4);
    camera.lookAt(20, 10, 20);
    controls.update();

    createGeometry();

    document.body.appendChild(renderer.domElement);

};

function animate() {
    requestAnimationFrame(animate)
    //controls.update()
    render()
}

function render() {
    renderer.render(scene, camera)
}

init();
animate();

